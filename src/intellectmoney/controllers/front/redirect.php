<?php

class intellectmoneyredirectModuleFrontController extends ModuleFrontController {

    private $orderCurrency;
    private $rubCurrency;
    
    public function initContent() {
        parent::initContent();
        $cart = New Cart($this->context->cart->id); // Объект корзины
        $intellectmoney = new intellectmoney(); //Объект intellectmoney
        $recipientAmount = $cart->getOrderTotal(true, Cart::BOTH); //Сумма заказа
        $intellectmoney->validateOrder($cart->id, Configuration::get('PS_OS_CHEQUE'), $recipientAmount, $intellectmoney->displayName); //Создание заказа с статусом ожидаем оплату
        $eshopId = Tools::safeOutput(Configuration::get('IM_eshopId')); //номер поставщика
        $secretKey = Tools::safeOutput(Configuration::get('IM_secretKey')); //ключ

        $recipientCurrency = Tools::safeOutput(Configuration::get('IM_recipientCurrency')); // Валюта из настроек модуля
        if ($recipientCurrency !== 'TST') {
            $recipientAmount = $this->convertAmount($recipientAmount, $cart);
            $recipientCurrency = "RUB";
        }
        $customer = new Customer((int) $cart->id_customer);
        $userEmail = $customer->email;
        $holdMode = Tools::safeOutput(Configuration::get('IM_holdMode'));
        if ($holdMode == true) {
            $day = Tools::safeOutput(Configuration::get('IM_expireDate'));
            $expireDate = date('Y-m-d H:i:00', strtotime('+' . $day . ' hours'));
            $holdMode = 1;
        } else {
            $holdMode = 0;
        }
        $control_hash_str = implode('::', array($eshopId, $cart->id, 'Оплата заказа №' . $cart->id, $recipientAmount, $recipientCurrency, $secretKey));
        $hash = md5($control_hash_str);
        //$customer = new Customer((int)$this->context->cart->id_customer);
        //$products = $cart->getProducts();//Описание продукта
        $this->context->smarty->assign(array(
            'eshopId' => $eshopId,
            'recipientAmount' => $recipientAmount,
            'orderId' => $cart->id,
            'serviceName' => 'Оплата заказа №' . $cart->id,
            'recipientCurrency' => $recipientCurrency,
            'userEmail' => $userEmail,
            'holdMode' => $holdMode,
            'expireDate' => $expireDate,
            'hash' => $hash,
            'UserField_1' => $this->orderCurrency->getConversionRate(),
            'UserFieldName_1' => "OrderCurrencyConversionRate",
            'UserField_2' => $this->rubCurrency->getConversionRate(),
            'UserFieldName_2' => "RubCurrencyConversionRate",
            'UserField_3' => $cart->getOrderTotal(true, Cart::BOTH),
            'UserFieldName_3' => "DefaultOrderAmount"
        ));
        $this->setTemplate('module:intellectmoney/views/templates/front/redirect.tpl');
    }

    private function convertAmount($recipientAmount, $cart) {
        $this->orderCurrency = new Currency($cart->id_currency);
        $this->rubCurrency = new Currency(Currency::getIdByIsoCode("RUB"));
        $defaultCurrency = new Currency(Configuration::get('PS_CURRENCY_DEFAULT'));

        if ($this->orderCurrency->id != $defaultCurrency->id) { // Если валюта заказа - это не валюта по умолчанию
            $recipientAmount = $recipientAmount * $this->orderCurrency->getConversionRate();
        }
        if ($this->orderCurrency->id != $this->rubCurrency->id) { // Если валюта заказа - это не рубли. Оба условия могут выполниться, это норм.
            $recipientAmount = $recipientAmount * $this->rubCurrency->getConversionRate();
        }

        return round($recipientAmount, 2, PHP_ROUND_HALF_UP);
    }

}

?>