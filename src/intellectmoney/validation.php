<?php

include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/intellectmoney.php');
include(dirname(__FILE__) . '/../../header.php');
include(dirname(__FILE__) . '/../../init.php');

function ob_exit($status = null) {
    if ($status) {
        ob_end_flush();
        exit($status);
    } else {
        ob_end_clean();
        header("HTTP/1.0 200 OK");
        echo "OK";
        exit();
    }
}

function check_im() {
    return in_array($_SERVER['REMOTE_ADDR'], array("194.147.107.254", "91.212.151.242", "127.0.0.1"));
}

ob_start();

$is_encoded = preg_match('~%[0-9A-F]{2}~i', $_REQUEST['serviceName']);
if ($is_encoded) {
    list($eshopId, $cartId, $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency,
            $paymentStatus, $userName, $userEmail, $paymentData, $secretKey, $hash,
            $orderCurrencyConversionRate, $rubCurrencyConversionRate, $defaultOrderAmount) = array(
        $_REQUEST['eshopId'], urldecode($_REQUEST['orderId']), urldecode($_REQUEST['serviceName']), $_REQUEST['eshopAccount'],
        $_REQUEST['recipientAmount'], $_REQUEST['recipientCurrency'], $_REQUEST['paymentStatus'], urldecode($_REQUEST['userName']),
        urldecode($_REQUEST['userEmail']), urldecode($_REQUEST['paymentData']), $_REQUEST['secretKey'], $_REQUEST['hash'],
        $_REQUEST['UserField_1'], $_REQUEST['UserField_2'], $_REQUEST['UserField_3']
    );
} else {
    list($eshopId, $cartId, $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency,
            $paymentStatus, $userName, $userEmail, $paymentData, $secretKey, $hash,
            $orderCurrencyConversionRate, $rubCurrencyConversionRate, $defaultOrderAmount) = array(
        $_REQUEST['eshopId'], $_REQUEST['orderId'], $_REQUEST['serviceName'], $_REQUEST['eshopAccount'],
        $_REQUEST['recipientAmount'], $_REQUEST['recipientCurrency'], $_REQUEST['paymentStatus'], $_REQUEST['userName'],
        $_REQUEST['userEmail'], $_REQUEST['paymentData'], $_REQUEST['secretKey'], $_REQUEST['hash'],
        $_REQUEST['UserField_1'], $_REQUEST['UserField_2'], $_REQUEST['UserField_3']
    );
}
$cartId = intval($cartId);

// Проверка по контрольной подписи
$control_hash_str = implode('::', array(
    Tools::safeOutput(Configuration::get('IM_eshopId')), $cartId, $serviceName,
    $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus,
    $userName, $userEmail, $paymentData, Tools::safeOutput(Configuration::get('IM_secretKey')),
        ));
$control_hash = md5($control_hash_str);
$control_hash_utf8 = md5(iconv('windows-1251', 'utf-8', $control_hash_str));

if (($hash != $control_hash && $hash != $control_hash_utf8) || !$hash) {
    $err = "ERROR: HASH MISMATCH\n";
    $err .= check_im() ? "Control hash string: $control_hash_str;\n" : "";
    $err .= "Control hash win-1251: $control_hash;\nControl hash utf-8: $control_hash_utf8;\nhash: $hash;\n\n";
    ob_exit($err);
}

//Проверка суммы
$cart = new Cart($cartId);
$cartAmount = $cart->getOrderTotal(true, Cart::BOTH);
if (
        $cartAmount != $defaultOrderAmount 
        && $cartAmount != $recipientAmount 
        && round($cartAmount * $rubCurrencyConversionRate, 2, PHP_ROUND_HALF_UP) != $recipientAmount 
        && round($cartAmount * $rubCurrencyConversionRate * $orderCurrencyConversionRate, 2, PHP_ROUND_HALF_UP) != $recipientAmount
) {
    ob_exit("ERROR: CURRENCY MISMATCH!");
}

$proceedSKOStatus = Configuration::get('IM_createdState');
$paidSKOStatus = Configuration::get('IM_payedState');
$refundSKOStatus = Configuration::get('IM_refundState');
$holdSKOStatus = Configuration::get('IM_holdState');

$history = new OrderHistory();
$orderId = Order::getOrderByCartId($cartId);
$order = new Order($orderId);
$orderStatus = $order->getCurrentState();
// Создан СКО
if ($paymentStatus == '3' && !in_array($orderStatus, array($paidSKOStatus, $refundSKOStatus, $holdSKOStatus))) {
    $history->changeIdOrderState($proceedSKOStatus, $orderId);
}
// Отменен СКО
if ($paymentStatus == '4') {
    $history->changeIdOrderState($refundSKOStatus, $orderId);
}
// Полностью оплачен
if ($paymentStatus == '5' && $orderStatus != $refundSKOStatus) {
    $history->changeIdOrderState($paidSKOStatus, $orderId);
}
// Захолдирован
if ($paymentStatus == '6' && !in_array($orderStatus, array($refundSKOStatus, $paidSKOStatus))) {
    $history->changeIdOrderState($holdSKOStatus, $orderId);
}
ob_exit();

?>