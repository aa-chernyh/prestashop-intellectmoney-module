<?php

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_'))
    exit;

class IntellectMoney extends PaymentModule {

    public function __construct() {
        $this->name = 'intellectmoney';
        $this->tab = 'payments_gateways';
        $this->author = 'IntellectMoney';
        $this->version = '7.0.0';
        $this->controllers = array('redirect');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        parent::__construct();

        $this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('IntellectMoney');
        $this->description = $this->l('Приём платежей через систему IntellectMoney');
        $this->confirmUninstall = $this->l('Вы уверены, что хотите удалить модуль IntellectMoney?');
    }

    // Установка модуля
    public function install() {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        return parent::install() &&
                $this->registerHook('paymentOptions') &&
                $this->registerHook('paymentReturn');
    }

    // Удаление модуля
    public function uninstall() {
        return parent::uninstall() &&
                Configuration::deleteByName('INTELLECTMONEY_MODULE_NAME') &&
                Configuration::deleteByName('eshopId') &&
                Configuration::deleteByName('holdMode') &&
                Configuration::deleteByName('mode') &&
                Configuration::deleteByName('expireDate') &&
                Configuration::deleteByName('createdState') &&
                Configuration::deleteByName('payedState') &&
                Configuration::deleteByName('refundState') &&
                Configuration::deleteByName('holdState') &&
                Configuration::deleteByName('secretKey');
    }

    // Сохранение значений из конфигурации
    public function getContent() {
        $output = null;
        $check = true;
        $array = array('eshopId', 'payedState', 'createdState', 'refundState', 'holdState', 'recipientCurrency', 'holdMode', 'expireDate', 'secretKey');

        if (Tools::isSubmit('submit' . $this->name)) {
            foreach ($array as $value) {
                $conf_value = strval(Tools::getValue($value));
                if (!$conf_value || is_null($conf_value) || !Validate::isGenericName($conf_value)) {
                    $output .= $this->displayError($this->l('Неверное значение поля'));
                    $check = false;
                } elseif ($check) {
                    Configuration::updateValue('IM_' . $value, $conf_value);
                }
            }
            if ($check) {
                $output .= $this->displayConfirmation($this->l('Настройки сохранены'));
            }
        }
        return $output . $this->displayForm();
    }

    // Форма страницы конфигурации
    public function displayForm() {
        $this->context->smarty->assign('orderStates', OrderState::getOrderStates($this->context->language->id));

        $this->context->smarty->assign('payedState', Configuration::get('IM_payedState'));
        $this->context->smarty->assign('createdState', Configuration::get('IM_createdState'));
        $this->context->smarty->assign('refundState', Configuration::get('IM_refundState'));
        $this->context->smarty->assign('holdState', Configuration::get('IM_holdState'));

        $this->context->smarty->assign('eshopId', Configuration::get('IM_eshopId'));
        $this->context->smarty->assign('secretKey', Configuration::get('IM_secretKey'));
        $this->context->smarty->assign('recipientCurrency', Configuration::get('IM_recipientCurrency'));
        $this->context->smarty->assign('holdMode', Configuration::get('IM_holdMode'));
        $this->context->smarty->assign('expireDate', Configuration::get('IM_expireDate'));

        if (empty($_SERVER['HTTPS'])) {
            $resultURL = 'http://' . $_SERVER['SERVER_NAME'] . '/modules/intellectmoney/validation.php';
        } else {
            $resultURL = 'https://' . $_SERVER['SERVER_NAME'] . '/modules/intellectmoney/validation.php';
        }
        $this->context->smarty->assign('resultURL', $resultURL);

        return $this->display(__FILE__, 'views/templates/admin/paymentConfiguration.tpl');
    }

    // Хук оплаты
    public function hookPaymentOptions($params) {

        if (!$this->active) {
            return;
        }

        $newOption = new PaymentOption();
        $newOption->setCallToActionText($this->l('IntellectMoney'))
                ->setAction($this->context->link->getModuleLink($this->name, 'redirect', array(), true))
                ->setLogo(Media::getMediaPath(_PS_MODULE_DIR_ . $this->name . '/logo.gif'));
        $payment_options = [
            $newOption,
        ];

        return $payment_options;
    }

    public function hookPaymentReturn($params) {
        if (!$this->active) {
            return;
        }

        $state = $params['order']->getCurrentState();
        $this->smarty->assign(array(
            'shop_name' => $this->context->shop->name,
            'total' => Tools::displayPrice(
                    $params['order']->getOrdersTotalPaid(), new Currency($params['order']->id_currency), false
            ),
            'status' => 'ok',
            'contact_url' => $this->context->link->getPageLink('contact', true)
        ));
        return $this->display(__FILE__, 'payment_return.tpl');
    }

}

?>