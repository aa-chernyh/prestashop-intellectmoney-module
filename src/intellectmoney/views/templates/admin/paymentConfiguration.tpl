<div class="bootstrap">
    <form id="module_form" class="defaultForm form-horizontal" action="{$currentIndex|escape:'htmlall':'UTF-8'}" method="post" enctype="multipart/form-data">
        <div class="panel">
            <div class="form-wrapper">
                <div class="form-group">
                    <label class="control-label col-lg-2 required">Номер магазина</label>
                    <div class="col-lg-10">
                        <input type="text" name="eshopId" value="{$eshopId}" required="required"/>
                        <p class="help-block">Номер магазина в системе Intellectmoney</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2 required">Секретный ключ</label>
                    <div class="col-lg-10">
                        <input type="password" name="secretKey" value="{$secretKey}" required="required"/>
                        <p class="help-block">Секретный ключ</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Тестовый режим</label>
                    <div class="col-lg-10">
                        <select name="recipientCurrency" class="fixed-width-xl">
                            <option value="TST" {if $recipientCurrency=="TST"}selected="selected"{/if}>Включено</option>
                            <option value="RUB" {if $recipientCurrency=="RUB"}selected="selected"{/if}>Выключено</option>
                        </select>                        
                        <p class="help-block">Тестовый режим с валютой TST</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Режим холдирования</label>
                    <div class="col-lg-10">
                        <select name="holdMode" class="fixed-width-xl">
                            <option value="true" {if $holdMode=="true"}selected="selected"{/if}>Включено</option>
                            <option value="false" {if $holdMode=="false"}selected="selected"{/if}>Выключено</option>
                        </select>                        
                        <p class="help-block">Режим холдирования</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Срок холдирования/жизни в часах</label>
                    <div class="col-lg-10">
                        <input type="text" name="expireDate" value="{$expireDate}" class="fixed-width-xl"/>
                        <p class="help-block">Срок холдирования в часах</p>
                    </div>
                </div>                        
                <div class="form-group">
                    <label class="control-label col-lg-2">Статус выставленных счетов</label>
                    <div class="col-lg-10">
                        <select name="createdState">
                            {foreach from=$orderStates item=order}
                                <option value="{$order['id_order_state']}" {if $createdState==$order['id_order_state']}selected="selected"{/if}>{$order['name']}</option>
                            {/foreach}
                        </select>
                        <p class="help-block">Статус созданных счетов</p>
                    </div>
                </div>                        
                <div class="form-group">
                    <label class="control-label col-lg-2">Статус оплаченных счетов</label>
                    <div class="col-lg-10">
                        <select name="payedState">
                            {foreach from=$orderStates item=order}
                                <option value="{$order['id_order_state']}" {if $payedState==$order['id_order_state']}selected="selected"{/if}>{$order['name']}</option>
                            {/foreach}
                        </select>
                        <p class="help-block">Статус оплаченных счетов</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Статус холдированных счетов</label>
                    <div class="col-lg-10">
                        <select name="holdState">
                            {foreach from=$orderStates item=order}
                                <option value="{$order['id_order_state']}" {if $holdState==$order['id_order_state']}selected="selected"{/if}>{$order['name']}</option>
                            {/foreach}
                        </select>
                        <p class="help-block">Статус холдированных счетов</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Статус отменных счетов</label>
                    <div class="col-lg-10">
                        <select name="refundState">
                            {foreach from=$orderStates item=order}
                                <option value="{$order['id_order_state']}" {if $refundState==$order['id_order_state']}selected="selected"{/if}>{$order['name']}</option>
                            {/foreach}
                        </select>
                        <p class="help-block">Статус отменных счетов</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">ResultURL</label>
                    <div class="col-lg-10">
                        <input type="text" name="resultURL" value="{$resultURL}" disabled/>    
                        <p class="help-block">URL адрес для отправки уведомлений</p>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>

            <div class="panel-footer"><center>
                    <button type="submit" value="1" name="submitintellectmoney" class="btn btn-default">
                        <i class="process-icon-save"></i> Сохранить настройки
                    </button>
                </center>
            </div>

        </div>
    </form>
</div>
