{if $status == 'ok'}
    <strong>{l s='Заказ будет доставлен, как только поступит оплата.' mod='IntellectMoney'}</strong>
{else}
    <p class="warning">
        {l s='Оплата отменена. Пожалуйста, повторите попытку позже.'}
    </p>
{/if}