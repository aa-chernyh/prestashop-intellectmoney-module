#Модуль оплаты платежной системы IntellectMoney для CMS Prestashop

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/Prestashop#prestashop_files.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/Prestashop#5578466207233110cd439bb241cf2d0edd7c35
